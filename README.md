[![Build Status](https://travis-ci.com/Hexlet/hexlet-correction.svg?branch=master)](https://travis-ci.com/Hexlet/hexlet-correction)
[![Maintainability](https://api.codeclimate.com/v1/badges/377ea817e9c8b7d9d6cf/maintainability)](https://codeclimate.com/github/Hexlet/hexlet-correction/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/377ea817e9c8b7d9d6cf/test_coverage)](https://codeclimate.com/github/Hexlet/hexlet-correction/test_coverage)

# hexlet-correction

Сервис для отправки с сайтов ошибок пользователями (к примеру, выделяем текст с ошибкой и отправляем через вызов формы Ctrl + Enter), для дальнейшей обработки клиентом сервиса.

### Участие

* Обсуждение в канале #java слака http://slack-ru.hexlet.io

### Требования к системе для запуска и разработки (без учета OS)

* Java 12
* mvn

### Стек технологий проекта

* Java 12
* Spring Boot 2
* PostgreSQL
* Frontend - ReactJs

### Компиляция и запуск

```bash
$ make
```

### MVP

* Регистрация пользователей сервиса
* Генерация JS кода для установки на сайт
* Отправка на сервис ошибок (через REST API)
* Возможность просмотра ошибок пользователем в интерфейсе сервиса

##
[![Hexlet Ltd. logo](https://raw.githubusercontent.com/Hexlet/hexletguides.github.io/master/images/hexlet_logo128.png)](https://ru.hexlet.io/pages/about?utm_source=github&utm_medium=link&utm_campaign=exercises-java)

This repository is created and maintained by the team and the community of Hexlet, an educational project. [Read more about Hexlet (in Russian)](https://ru.hexlet.io/pages/about?utm_source=github&utm_medium=link&utm_campaign=exercises-java).
##