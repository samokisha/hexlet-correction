package io.hexlet.hexletcorrection.controller;

class ControllerConstrainConstants {

    final static String TEST_HOST = "http://localhost";

    final static String CORRECTIONS_PATH = "/corrections";

    final static String USER_PATH = "/users";

}
